"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv").config();
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const body_parser_1 = __importDefault(require("body-parser"));
const path_1 = __importDefault(require("path"));
const Api_1 = __importDefault(require("./routes/Api"));
const data_source_1 = __importDefault(require("./database/data-source"));
class App {
    constructor() {
        this.reactBuild = path_1.default.join(__dirname, 'react_build');
        this.app = (0, express_1.default)();
        this.app.use(express_1.default.static(this.reactBuild));
        this.app.use((0, body_parser_1.default)());
        this.app.use(body_parser_1.default.json());
        this.app.use(body_parser_1.default.urlencoded());
        this.app.use(body_parser_1.default.urlencoded({ extended: true }));
        this.app.use((0, cors_1.default)());
        (0, Api_1.default)(this.app); // Register API routes.
        this.app.get('*', (req, res) => __awaiter(this, void 0, void 0, function* () {
            res.sendFile(path_1.default.join(this.reactBuild, 'index.html'));
        }));
    }
    /**
     * @desc - Call to run the server.
     */
    run(port) {
        this.app.listen(port, () => __awaiter(this, void 0, void 0, function* () {
            yield data_source_1.default.initialize(); // Connect to database.
        }));
    }
}
/**
 * @desc - It's like the main function.
 */
(() => {
    const port = process.env.APP_PORT;
    const app = new App();
    app.run(port);
})();
//# sourceMappingURL=App.js.map