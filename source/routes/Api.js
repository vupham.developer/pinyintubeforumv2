"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv").config();
const UserController_1 = __importDefault(require("../controllers/UserController"));
const PostController_1 = __importDefault(require("../controllers/PostController"));
const Authenticate_1 = __importDefault(require("../middleware/Authenticate"));
function Api(app) {
    const apiEndpoint = "/api";
    // UserController.
    app.post(`${apiEndpoint}/users/login`, UserController_1.default.getAccessToken);
    app.post(`${apiEndpoint}/users`, UserController_1.default.create);
    // PostController.
    app.get(`${apiEndpoint}/posts`, PostController_1.default.getMany);
    app.get(`${apiEndpoint}/posts/:postId`, PostController_1.default.getOne);
    app.post(`${apiEndpoint}/posts`, Authenticate_1.default, PostController_1.default.create);
    app.post(`${apiEndpoint}/comments/:commentId/replies`, Authenticate_1.default, PostController_1.default.addReply);
    app.post(`${apiEndpoint}/posts/:postId/comments`, Authenticate_1.default, PostController_1.default.addComment);
}
exports.default = Api;
//# sourceMappingURL=Api.js.map