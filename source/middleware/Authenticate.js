"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv").config();
const data_source_1 = __importDefault(require("../database/data-source"));
const User_1 = __importDefault(require("../models/User"));
function authenticate(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        const authEmail = req.query.email;
        if (authEmail) {
            let foundUser = yield data_source_1.default
                .getRepository(User_1.default)
                .createQueryBuilder("user")
                .where("user.email = :email", { email: authEmail })
                .getOne();
            if (!foundUser) {
                console.log("hahahahah");
                foundUser = new User_1.default();
                foundUser.email = String(authEmail);
                yield data_source_1.default
                    .createQueryBuilder()
                    .insert()
                    .into(User_1.default)
                    .values(foundUser)
                    .execute();
            }
            req.body.user = foundUser;
            next();
        }
        else {
            res.status(403).send({ message: "Unauthorized." });
        }
    });
}
exports.default = authenticate;
//# sourceMappingURL=Authenticate.js.map