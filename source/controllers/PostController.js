"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv").config();
const data_source_1 = __importDefault(require("../database/data-source"));
const Post_1 = __importDefault(require("../models/Post"));
const Comment_1 = __importDefault(require("../models/Comment"));
const Rely_1 = __importDefault(require("../models/Rely"));
const email_1 = __importDefault(require("../utils/email"));
class PostController {
    static getMany(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const posts = yield data_source_1.default
                .getRepository(Post_1.default)
                .createQueryBuilder('post')
                .innerJoinAndSelect('post.user', 'user')
                .leftJoinAndSelect('post.comments', 'comment')
                .leftJoinAndSelect('comment.replies', 'reply')
                .orderBy('post.createdAt', 'DESC')
                .getMany();
            res.status(200).send(posts);
        });
    }
    static addReply(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const commentId = req.params.commentId;
            const foundComment = yield data_source_1.default
                .getRepository(Comment_1.default)
                .createQueryBuilder("comment")
                .innerJoinAndSelect('comment.user', 'user')
                .innerJoinAndSelect('comment.post', 'post')
                .where("comment.id = :id", { id: commentId })
                .getOne();
            // console.log(foundComment)
            const newReply = new Rely_1.default();
            newReply.comment = foundComment;
            newReply.user = req.body.user;
            newReply.content = req.body.content;
            yield data_source_1.default
                .createQueryBuilder()
                .insert()
                .into(Rely_1.default)
                .values(newReply)
                .execute();
            let email = {
                to: foundComment.user.email,
                subject: '[PinyinTube-Forum] some on has replied your comment',
                text: `some one has replied your comment`,
                html: `<p><a style="padding: 10px 20px; border-radius: 20px; color: white; font-size: 18px; background: #328AF1; text-decoration: none;" href="${process.env.DOMAIN}/posts/${foundComment.post.id}#${newReply.id}">see now!</a></p>`
            };
            (0, email_1.default)(email);
            res.status(201).send({ message: "Created Successfully." });
        });
    }
    static addComment(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const postId = req.params.postId;
            const foundPost = yield data_source_1.default
                .getRepository(Post_1.default)
                .createQueryBuilder("post")
                .innerJoinAndSelect('post.user', 'user')
                .where("post.id = :id", { id: postId })
                .getOne();
            const newComment = new Comment_1.default();
            newComment.post = foundPost;
            newComment.user = req.body.user;
            newComment.content = req.body.content;
            yield data_source_1.default
                .createQueryBuilder()
                .insert()
                .into(Comment_1.default)
                .values(newComment)
                .execute();
            let email = {
                to: foundPost.user.email,
                subject: '[PinyinTube-Forum] some on has commented on your post',
                text: `some on has commented on your post`,
                html: `<p><a style="padding: 10px 20px; border-radius: 20px; color: white; font-size: 18px; background: #328AF1; text-decoration: none;" href="${process.env.DOMAIN}/posts/${foundPost.id}">see now!</a></p>`
            };
            (0, email_1.default)(email);
            res.status(201).send({ message: "Created Successfully." });
        });
    }
    static getOne(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const postId = req.params.postId;
            const foundPost = yield data_source_1.default
                .getRepository(Post_1.default)
                .createQueryBuilder("post")
                .innerJoinAndSelect('post.user', 'user')
                .leftJoinAndSelect('post.comments', 'comment')
                .leftJoinAndSelect('comment.user', 'comment-replier')
                .leftJoinAndSelect('comment.replies', 'reply')
                .leftJoinAndSelect('reply.user', 'reply-replier')
                .where("post.id = :id", { id: postId })
                .orderBy('comment.createdAt', 'DESC')
                .addOrderBy('reply.createdAt', 'DESC')
                .getOne();
            if (foundPost)
                res.status(200).send(foundPost);
            else
                res.status(404).send({ message: "Not found." });
        });
    }
    static create(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            yield data_source_1.default
                .createQueryBuilder()
                .insert()
                .into(Post_1.default)
                .values({
                title: req.body.title,
                content: req.body.content,
                user: req.body.user
            })
                .execute();
            res.status(201).send(req.body.user);
        });
    }
}
exports.default = PostController;
//# sourceMappingURL=PostController.js.map